import {firebaseConfig, componentFromConfig} from './env.js'
import './radio4000-application-router.js'
import './radio4000-nav-main.js'
import './radio4000-player-container.js'
import '../firebase/app.js'

class Radio4000Cms extends HTMLElement {
	constructor() {
		super()
	}
	connectedCallback() {
		this.render()
	}

	render() {
		const $firebaseDatabase = componentFromConfig('firebase-database', firebaseConfig)
		$firebaseDatabase.hidden = true

		const $notificationSystem = document.createElement('notification-system')
		$notificationSystem.setAttribute('position', 'bottom-left')

		const $applicationRouter = document.createElement('radio4000-application-router')

		const $navMain = document.createElement('radio4000-nav-main')

		const $player = document.createElement('radio4000-player-container')
		$player.channelSlug = 'ko002'

		this.append($firebaseDatabase)
		this.append($notificationSystem)
		this.append($navMain)
		this.append($applicationRouter)
		this.append($player)
	}
}

customElements.define('radio4000-cms', Radio4000Cms)

export default Radio4000Cms
