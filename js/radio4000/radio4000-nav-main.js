import {algoliaConfig, componentFromConfig} from './env.js'
import { page, buildLink } from './router.js'
import '../firebase/auth-status.js'
import './radio4000-search.js'

class Radio4000NavMain extends HTMLElement {
	nav = [
		{
			title: 'Home',
			href: '/'
		},
		{
			title: 'Account',
			href: '/account',
			showAuth: true
		},
		{
			title: 'Channels',
			href: '/channels'
		},
		{
			title: 'New channel',
			href: '/new'
		}
	]

	connectedCallback() {
		this.sitePathname = this.getAttribute('site-pathname')
		this.render()
	}

	render() {
		const $component = this
		$component.innerHTML = ''

		this.nav.forEach(navItem => {
			let $navItem = document.createElement('nav-item')
			let $link = buildLink(navItem)

			if (navItem.showAuth) {
				let $authStatus = document.createElement('auth-status')
				$authStatus.setAttribute('quiet', true)
				$authStatus.setAttribute('visual', true)
				$link.append($authStatus)
			}

			$navItem.append($link)
			$component.append($navItem)
		})

		// Add search input.
		const $search = componentFromConfig('radio4000-search', algoliaConfig)
		$component.append($search)

		// Change channel when you select something.
		$search.addEventListener('selected', ({detail}) => {
			const $player = document.querySelector('radio4000-player')
			console.log(`selected`, detail.suggestion)
			$player.channelSlug = detail.suggestion.slug
		})

	}
}

customElements.define('radio4000-nav-main', Radio4000NavMain)

export default Radio4000NavMain
