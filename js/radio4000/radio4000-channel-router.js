import './radio4000-channel.js'

class Radio4000ChannelRouter extends HTMLElement {
	constructor() {
		super()
	}
	connectedCallback() {
		this.slug = this.getAttribute('slug')
		this.render()
	}

	render() {
		let $channel = document.createElement('radio4000-channel')
		$channel.setAttribute('model-slug', this.slug)
		this.append($channel)
	}
}

customElements.define('radio4000-channel-router', Radio4000ChannelRouter)

export default Radio4000ChannelRouter
