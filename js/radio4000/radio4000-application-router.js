import {page} from './router.js'
import '../firebase/auth-flow.js'
import './radio4000-page-home.js'
import './radio4000-page-new.js'
import './radio4000-channels.js'
import './radio4000-channel-router.js'

class ApplicationRouter extends HTMLElement {
	constructor() {
		super()
	}
	connectedCallback() {
		this.defineRoutes()
		page(window.location.pathname)
	}

	defineRoutes = () => {
		page('/', () => {
			this.renderHomePage()
		})

		page('/account', () => {
			this.renderAccountPage()
		})

		page('/new', () => {
			this.renderNewChannelPage()
		})

		page('/channels', () => {
			this.renderChannelsPage()
		})

		page('/:channel', (context) => {
			const {channel} = context.params
			if (channel) {
				this.renderChannelPage(channel)
			}
		})
	}

	renderHomePage() {
		this.innerHTML = ``
		let $pageHome = document.createElement('radio4000-page-home')
		this.append($pageHome)
	}

	renderNewChannelPage () {
		this.innerHTML = ``
		let $pageNewChannel = document.createElement('radio4000-page-new')
		this.append($pageNewChannel)
	}

	renderChannelsPage() {
		this.innerHTML = ``
		let $latestChannels = document.createElement('radio4000-channels')
		$latestChannels.setAttribute('model-name', 'channels')
		$latestChannels.setAttribute('limit', 10)
		this.append($latestChannels)
	}
	renderAccountPage() {
		this.innerHTML = ``
		let $authFlow = document.createElement('auth-flow')
		this.append($authFlow)
	}
	renderChannelPage(channelSlug) {
		this.innerHTML = ``
		let $channelRouter = document.createElement('radio4000-channel-router')
		$channelRouter.setAttribute('slug', channelSlug)
		this.append($channelRouter)
	}
}

customElements.define('radio4000-application-router', ApplicationRouter)

export default ApplicationRouter
