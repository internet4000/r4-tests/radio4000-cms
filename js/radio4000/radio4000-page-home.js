import '../firebase/with-auth.js'
import '../firebase/with-database.js'
import '../firebase/with-auth-database.js'

class Radio4000PageHome extends HTMLElement {
	constructor() {
		super()
	}
	connectedCallback() {
		this.render()
	}

	render() {
		let $withDatabase = document.createElement('with-database')
		let $withAuth = document.createElement('with-auth')
		let $withAuthDatabase = document.createElement('with-auth-database')
		this.append($withDatabase)
		this.append($withAuth)
		this.append($withAuthDatabase)
	}
}

customElements.define('radio4000-page-home', Radio4000PageHome)

export default Radio4000PageHome
