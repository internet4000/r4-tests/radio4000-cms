class Radio4000ChannelHeader extends HTMLElement {
	constructor() {
		super()
	}
	connectedCallback() {
		/* this.render() */
	}

	render() {
		let $channel = document.createElement('radio4000-channel')
		this.append($channel)
	}
}

customElements.define('radio4000-channel-header', Radio4000ChannelHeader)

export default Radio4000ChannelHeader
