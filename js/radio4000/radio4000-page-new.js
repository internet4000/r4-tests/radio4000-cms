import '../models/channel/create.js'

class Radio4000PageNew extends HTMLElement {
	constructor() {
		super()
	}
	connectedCallback() {
		this.render()
	}

	render() {
		let $channel = document.createElement('channel-create')
		this.append($channel)
	}
}

customElements.define('radio4000-page-new', Radio4000PageNew)

export default Radio4000PageNew
