export default class Radio4000Search extends HTMLElement {
	connectedCallback() {
		this.applicationId = this.getAttribute('application-id')
		this.apiKey = this.getAttribute('api-key')

		this.randomId = 'cake' + Math.random().toString(36).substring(2, 15)
		this.loadAlgolia()
		this.render()
	}
	// Include AlgoliaSearch JS Client and autocomplete.js library,
	// and enable algolia when loaded.
	loadAlgolia() {
		// if (typeof window.algoliasearch !== 'undefined') return this.render()
		// Call the callback once both scripts are loaded.
		let ready = 0
		const callback = () => {
			ready++
			if (ready === 2) this.enableAlgolia()
		}
		const loadScript = (src) => {
			var script = document.createElement('script')
			script.src = src
			script.onload = script.onreadystatechange = callback
			document.body.append(script)
		}
		loadScript('https://cdn.jsdelivr.net/algoliasearch/3/algoliasearchLite.min.js')
		loadScript('https://cdn.jsdelivr.net/npm/autocomplete.js@0.37.1/dist/autocomplete.min.js')
	}
	enableAlgolia() {
		var self = this
		var client = algoliasearch(this.applicationId, this.apiKey)
		var index = client.initIndex('radio4000_channels')

		autocomplete(`#${this.randomId}`, {autoselect: true, hint: true}, [{
			source: autocomplete.sources.hits(index, {hitsPerPage: 10}),
			displayKey: 'title',
			templates: {
				// Modify the HTML template for each suggestion.
				suggestion(model) {
					const img = `<img width="30" src="https://res.cloudinary.com/radio4000/image/upload/w_50,h_50,c_thumb,q_60/${model.image}" alt="">`
					return `
						${typeof model.image !== undefined && img}
						<span>${model.title}</span>
					`
				}
			}
		}]).on('autocomplete:selected', function(event, suggestion, dataset) {
			const evt = new CustomEvent('selected', {detail: {suggestion, dataset}})
			self.dispatchEvent(evt)
		})
	}

	render() {
		this.innerHTML = `
				<input
					type="search"
					id=${this.randomId}
					class="aa-search"
					placeholder="Search Radio4000 channels…"
					autocomplete="off"
				/>
				<svg class="aa-input-icon" viewBox="654 -372 1664 1664">
					<path
						d="M1806,332c0-123.3-43.8-228.8-131.5-316.5C1586.8-72.2,1481.3-116,1358-116s-228.8,43.8-316.5,131.5  C953.8,103.2,910,208.7,910,332s43.8,228.8,131.5,316.5C1129.2,736.2,1234.7,780,1358,780s228.8-43.8,316.5-131.5  C1762.2,560.8,1806,455.3,1806,332z M2318,1164c0,34.7-12.7,64.7-38,90s-55.3,38-90,38c-36,0-66-12.7-90-38l-343-342  c-119.3,82.7-252.3,124-399,124c-95.3,0-186.5-18.5-273.5-55.5s-162-87-225-150s-113-138-150-225S654,427.3,654,332  s18.5-186.5,55.5-273.5s87-162,150-225s138-113,225-150S1262.7-372,1358-372s186.5,18.5,273.5,55.5s162,87,225,150s113,138,150,225  S2062,236.7,2062,332c0,146.7-41.3,279.7-124,399l343,343C2305.7,1098.7,2318,1128.7,2318,1164z"
					/>
				</svg>
		`
	}
}

customElements.define('radio4000-search', Radio4000Search)
