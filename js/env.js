const isDevelopment = window.location.origin.includes('localhost:')

/* for gitlab pages hosting internet4000.gitlab.io/radio4000-cms */
const sitePathname =  isDevelopment ? '' : '/radio4000-cms'
export {
    isDevelopment,
    sitePathname
}
