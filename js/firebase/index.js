import App from './app.js'
import Register from './register.js'
import Login from './login.js'
import Logout from './logout.js'
import User from './user.js'
import DeleteUser from './delete-user.js'
import AuthFlow from './auth-flow.js'
import AuthStatus from './auth-status.js'

export default {}
