const serializeList = (dataSnapshot) => {
	const values = dataSnapshot.val()
	if (!values) return []
	const sValues = Object.entries(values).map((entry) => {
		const [id, item] = entry
		item.id = id
		return item
	})
	return sValues
}

const serializeItem = (dataSnapshot) => {
	return {
		id: dataSnapshot.key,
		...dataSnapshot.val()
	}
}

export {
	serializeList,
	serializeItem
}
