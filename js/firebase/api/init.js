/*
   public method to init firebase app,
   and get a firebase instance with our config */
const initFirebaseApp = async (config, callback, options) => {
	const {
		firebaseAuth,
		firebaseRealtime,
		firebaseFirestore,
		firebaseFunctions
	} = options

  if (window.firebase) {
		return callback
	}

	const firebaseApp = await createFirebaseScript(config)

	if (firebaseAuth) {
		await createFirebaseAuthScript()
	}

	if (firebaseFirestore) {
		await createDatabaseFirestoreScript(() => {
		})
	}

	if (firebaseRealtime) {
		await createDatabaseRealtimeScript(() => {
		})
	}

	if (firebaseFunctions) {
		await createFirebaseFunctionsScript(() => {
		})
	}

	callback(firebaseApp)
}

/* insert firebase script and give callback to onload */
const createFirebaseScript = (config, callback) => {
	return new Promise((resolv, reject) => {
		const firebaseScript = document.createElement('script')
		firebaseScript.src = 'https://www.gstatic.com/firebasejs/7.14.2/firebase-app.js'
		firebaseScript.async = true
		firebaseScript.onload = () => {
			const firebaseApp = firebase.initializeApp(config)
			resolv(firebaseApp)
		}
		document.body.appendChild(firebaseScript)
	})
}

/* we also need firebase auth, give it the callback,
   for when it is ready */
const createFirebaseAuthScript = () => {
	return new Promise((resolv, reject) => {
		const firebaseAuthScript = document.createElement('script')
		firebaseAuthScript.src = 'https://www.gstatic.com/firebasejs/7.14.4/firebase-auth.js'
		firebaseAuthScript.async = true
		firebaseAuthScript.onload = () => resolv()
		document.body.appendChild(firebaseAuthScript)
	})
}

/* we also need firestore, give it the callback,
   for when it is ready */
const createDatabaseFirestoreScript = (callback) => {
	return new Promise((resolv, reject) => {
		const firestoreScript = document.createElement('script')
		firestoreScript.src = 'https://www.gstatic.com/firebasejs/7.14.4/firebase-firestore.js'
		firestoreScript.async = true
		firestoreScript.onload = () => resolv()
		document.body.appendChild(firestoreScript)
	})
}

const createDatabaseRealtimeScript = (callback) => {
  return new Promise((resolv, reject) => {
		const realtimeScript = document.createElement('script')
		realtimeScript.src = 'https://www.gstatic.com/firebasejs/7.14.4/firebase-database.js'
		realtimeScript.async = true
		realtimeScript.onload = () => resolv()
		document.body.appendChild(realtimeScript)
	})
}

const createFirebaseFunctionsScript = (callback) => {
  return new Promise((resolv, reject) => {
		const firebaseFunctionsScript = document.createElement('script')
		firebaseFunctionsScript.src = 'https://www.gstatic.com/firebasejs/7.14.4/firebase-functions.js'
		firebaseFunctionsScript.async = true
		firebaseFunctionsScript.onload = () => resolv()
		document.body.appendChild(firebaseFunctionsScript)
	})
}

export {
  initFirebaseApp
}
