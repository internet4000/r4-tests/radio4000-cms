class WithDatabase extends HTMLElement {
	databaseDomSelector = 'firebase-app'
	databaseWindowSelector = 'firebase'

  connectedCallback() {
		const $databaseComponent = document.querySelector(this.databaseDomSelector)
		const databaseInstance = window[this.databaseWindowSelector]

		if (this.databaseWindowSelector && databaseInstance) {
			this.databaseCallback({
				detail: databaseInstance
			})
		}

		if ($databaseComponent) {
			$databaseComponent.addEventListener('firebaseReady', this.databaseCallback, false)
		}
	}

	disconnectedCallback() {
		const $databaseComponent = document.querySelector(this.databaseDomSelector)
		if ($databaseComponent) {
			$databaseComponent.removeEventListener('firebaseReady', this.databaseCallback, false)
		}
	}

	databaseCallback(event) {
		if (event && event.detail) {
			this.firebase = event.detail
		}
	}
}

customElements.define('with-database', WithDatabase)

export default WithDatabase
