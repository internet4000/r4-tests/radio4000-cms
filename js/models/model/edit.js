import {
	onModel,
	editModel
} from '../../firebase/api/index.js'
import './form.js'

class ModelEdit extends HTMLElement {
	beforeConnectedCallback = () => {}
	connectedCallback() {
		this.beforeConnectedCallback()

		this.submitText = this.getAttribute('submit-text') || 'Edit'
		this.modelType = this.getAttribute('model-type') || ''
		this.modelId = this.getAttribute('model-id') || ''

		this.quiet = this.getAttribute('quiet') || false

		const $firebaseApp = document.querySelector('firebase-app')

		if (window.firebase) {
			this.handleDatabaseReady({
				detail: window.firebase
			})
			this.handleUserLoggedIn({
				detail: window.firebase.auth().currentUser
			})
		}

		if ($firebaseApp) {
			$firebaseApp.addEventListener('firebaseReady', this.handleDatabaseReady, false)
			$firebaseApp.addEventListener('userLoggedIn', this.handleUserLoggedIn, false)
			this.addEventListener('modelSubmitted', this.handleModelSubmitted, false)
		}
	}

	disconnectedCallback() {
		const $firebaseApp = document.querySelector('firebase-app')
		if ($firebaseApp) {
			$firebaseApp.removeEventListener('firebaseReady', this.handleDatabaseReady)
			$firebaseApp.removeEventListener('userLoggedIn', this.handleUserLoggedIn)
			this.removeEventListener('modelSubmitted', this.handleModelSubmitted)
		}
	}

	handleDatabaseReady = async ({detail}) => {
		this.firebase = detail

		let model
		try {
			model = await onModel(this.firebase, this.modelType, this.modelId)
		} catch (error) {
			console.log('Error getting model', error)
		}
		this.model = model
		this.render()
	}

	handleUserLoggedIn = ({detail}) => {
		this.user = detail
		this.render()
	}

	handleModelSubmitted = async ({detail}) => {
		let model
		try {
			model = await editModel(this.firebase, this.modelType, this.modelId, detail)
		} catch (error) {
			console.log('error editing model', error)
		}
	}

	render() {
		let $component = this
		$component.innerHTML = ''

		if (!this.user) {
			const messageUserRequired = document.createElement('p')
			messageUserRequired.innerHTML = 'You need to be logged in to edit a model.'
			!this.quiet && $component.appendChild(messageUserRequired)
			return
		}

		if (!this.model
				|| !Object.keys(this.model).length) {
			const messageJobRequired = document.createElement('p')
			messageJobRequired.innerHTML = 'Cannot edit; no model exists with this model-id.'
			!this.quiet && $component.appendChild(messageJobRequired)
			return false
		}

		if (this.model.user
				&& this.model.user !== this.user.uid) {
			const messageUserOwner = document.createElement('p')
			messageUserOwner.innerHTML = 'You do not have the rights to edit this model.'
			$component.appendChild(messageUserOwner)
			return
		}

		const {
			title,
			url,
			body
		} = this.model

		const $form = document.createElement('form-model')
		$form.setAttribute('model-id', this.modelId)
		$form.setAttribute('submit-text', this.submitText)
		$form.setAttribute('title', title)
		$form.setAttribute('body', body)
		$component.appendChild($form)
	}
}

/* here as example; how to define a custom-element web component */
customElements.define('model-edit', ModelEdit)

export default ModelEdit
