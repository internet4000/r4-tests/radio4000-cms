class ModelItem extends HTMLElement {
	setInitialAttributes = () => {
		this.modelId = this.getAttribute('model-id') || ''
		this.title = this.getAttribute('title') || 'Untitled'
		this.url = this.getAttribute('url') || '#'
	}

	connectedCallback() {
		this.render()
	}
	render() {
		const $component = this

		let $itemLink = document.createElement('a')
		$itemLink.setAttribute('href', this.titleUrl || this.url)
		$itemLink.setAttribute('class', 'Title')
		$itemLink.innerText = this.title
		$component.append($itemLink)
	}
}

customElements.define('model-item', ModelItem)

export default ModelItem
