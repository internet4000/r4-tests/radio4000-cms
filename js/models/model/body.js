/*
	 params:
	 - body: some text content
 */

class ModelBody extends HTMLElement {
	connectedCallback() {
		this.body = this.getAttribute('body') || ''
		this.render()
	}
	render() {
		const $component = this
		let modelContent = document.createElement('main')
		modelContent.innerText = this.body
		$component.append(modelContent)
	}
}

customElements.define('model-body', ModelBody)

export default ModelBody
