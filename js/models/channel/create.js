import {
	createChannel
} from '../../firebase/api/index.js'
import {
	page
} from '../../router/index.js'

import ModelCreate from '../model/create.js'
import './form.js'

class ChannelCreate extends ModelCreate {
	beforeConnectedCallback = () => {
		this.setAttribute('model-type', 'channels')
	}
	get formElement() {
		return 'form-channel'
	}

	async createModel(firebaseInstance, modelType, modelData) {
		let channel
		try {
			channel = await createChannel(firebaseInstance, modelData)
		} catch(error) {
			console.log('error creating model', error)
			return
		}
		console.log('channel', channel)
		return channel
	}

	async afterCreateModel({slug}) {
		await page(`/${slug}`)
	}
}

customElements.define('channel-create', ChannelCreate)

export default ChannelCreate
