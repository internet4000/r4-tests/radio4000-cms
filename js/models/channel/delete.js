import ModelDelete from '../model/delete.js'

class ChannelDelete extends ModelDelete {
	beforeConnectedCallback = () => {
		this.setAttribute('model-type', 'channels')
	}
}

customElements.define('channel-delete', ChannelDelete)

export default ChannelDelete
